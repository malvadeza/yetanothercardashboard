package io.github.malvadeza.yetanothercardashboard;

/**
 * Created by Antonio on 10/08/2015.
 */

public class Constants {
    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_CONNECTING_DEVICE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_CONNECTED_DEVICE = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_STATE_CHANGE = 6;
    public static final int MESSAGE_SPEED = 7;
    public static final int MESSAGE_RPM = 8;
    public static final int MESSAGE_THROTTLE = 9;
    public static final int MESSAGE_TEMP = 10;

    // Key names received from the BluetoothChatService Handler
    public static final String CONNECTED_DEVICE = "io.github.com.malvadeza.testdrive.messages.CONNECTED_DEVICE";
    public static final String BT_RESPONSE = "io.github.com.malvadeza.testdrive.messages.BT_RESPONSE";
    public static final String TOAST = "io.github.com.malvadeza.testdrive.messages.TOAST";
    public static final String CONNECTING_DEVICE = "io.github.com.malvadeza.testdrive.messages.CONNECTING_DEVICE";
    public static final String STATE = "io.github.com.malvadeza.testdrive.messages.STATE";
}
