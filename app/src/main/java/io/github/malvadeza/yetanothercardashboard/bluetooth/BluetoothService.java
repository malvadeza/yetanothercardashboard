package io.github.malvadeza.yetanothercardashboard.bluetooth;

/**
 * Created by Antonio on 10/08/2015.
 */

import io.github.malvadeza.yetanothercardashboard.Constants;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class BluetoothService {
    private static final String TAG = BluetoothService.class.getSimpleName();

    public static final int MESSAGE_CONNECTING_DEVICE = 1;
    public static final int MESSAGE_CONNECT_DEVICE = 4;
    public static final int MESSAGE_STATE_CHANGE = 6;

    public static final int STATE_NOT_CONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    private Handler handler;

    private int state = 0;

    private BluetoothAdapter btAdapter;

    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final UUID MY_UUID_INSECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private ConnectThread connectThread;
//    private ConnectedThread connectedThread;

    private BluetoothSocket btSocket;

    public BluetoothService(Handler handler) {
        this.handler = handler;
        this.btAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public void connect(BluetoothDevice btDevice) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.CONNECTING_DEVICE, btDevice.getName() + ":" + btDevice.getAddress());

        Message msg = handler.obtainMessage(Constants.MESSAGE_CONNECTING_DEVICE);
        msg.setData(bundle);

        handler.sendMessage(msg);

        setState(STATE_CONNECTING);

        connectThread = new ConnectThread(btDevice);
        connectThread.start();
    }

    private void connected(BluetoothSocket btSocket, BluetoothDevice btDevice) {
        disconnect();

//        connectedThread = new ConnectedThread(btSocket);
//        connectedThread.start();
        this.btSocket = btSocket;

        Bundle bundle = new Bundle();
        bundle.putString(Constants.CONNECTED_DEVICE, btDevice.getName());

        Message msg = handler.obtainMessage(Constants.MESSAGE_CONNECTED_DEVICE);
        msg.setData(bundle);

        handler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    public BluetoothSocket getSocket() {
        return btSocket;
    }

    public void disconnect() {
        if ( connectThread != null ) {
            connectThread.cancel();
            connectThread = null;
        }

//        if (connectedThread != null) {
//            connectedThread.cancel();
//            connectedThread = null;
//        }

        setState(STATE_NOT_CONNECTED);
    }

    private synchronized void setState(int state) {
        Log.d(TAG, "BluetoothService.setState() -> " + this.state + " -> " + state);
        this.state = state;

        // Give the new state to the Handler so the UI Activity can update
        handler.obtainMessage(Constants.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    private class ConnectThread extends Thread {
        private BluetoothDevice btDevice;
        private BluetoothSocket btSocket;

        public ConnectThread(BluetoothDevice btDevice) {
            this.btDevice = btDevice;

            try {
                btSocket = btDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread() -> Socket creation failed", e);
            }
        }

        @Override
        public void run() {
            Log.d(TAG, "Beginning ConnectThread");

            btAdapter.cancelDiscovery();

            try {
                btSocket.connect();
            } catch (IOException e) {
                Log.e(TAG, "ConnectTread.run() -> Socket connection failed", e);

                try {
                    btSocket.close();
                } catch (IOException e1) {
                    Log.e(TAG, "ConnectTread.run() -> Unable to close socket", e1);
                }

                return;
            }

            synchronized (BluetoothService.this) {
                connectThread = null;
            }

            connected(btSocket, btDevice);
        }

        public void cancel() {
            try {
                btSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread.cancel() -> Unable to close socket");
            }
        }

    }

    private void connectionLost() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Device connection was lost");

        Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
        msg.setData(bundle);

        handler.sendMessage(msg);

//        Start the service over to restart listening mode
//        BluetoothChatService.this.start();
    }
}

