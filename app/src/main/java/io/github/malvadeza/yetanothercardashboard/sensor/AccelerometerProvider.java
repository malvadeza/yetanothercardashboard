package io.github.malvadeza.yetanothercardashboard.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by Antonio on 04/08/2015.
 */
public class AccelerometerProvider implements SensorEventListener {

    public final static String TAG = AccelerometerProvider.class.getSimpleName();

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private AccelerometerListener mAccelerometerListener;

    private Context mContext;

    public interface AccelerometerListener {
        public void onAccelerometerChanged(SensorEvent event);
    }

    public AccelerometerProvider(Context context) {
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mContext = context;
    }

    public void connect() {
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void disconnect() {
        mSensorManager.unregisterListener(this);
    }

    public void addSensorCallback(AccelerometerListener accelerometerListener) {
        mAccelerometerListener = accelerometerListener;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mAccelerometerListener.onAccelerometerChanged(event);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
