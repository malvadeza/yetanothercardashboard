package io.github.malvadeza.yetanothercardashboard;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.hardware.SensorEvent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import io.github.malvadeza.yetanothercardashboard.bluetooth.BluetoothService;
import io.github.malvadeza.yetanothercardashboard.bluetooth.OBD;
import io.github.malvadeza.yetanothercardashboard.sensor.AccelerometerProvider;
import io.github.malvadeza.yetanothercardashboard.sensor.LocationProvider;

public class MainActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        LocationProvider.NewLocationCallback,
        AccelerometerProvider.AccelerometerListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private LocationProvider mLocationProvider;
    private AccelerometerProvider mSensorProvider;

    private BluetoothAdapter mBtAdapter;
    private BluetoothService mBtService;
    private OBD mObdHandler;

    private GoogleMap mGoogleMap;

    private TextView mLatitude;
    private TextView mLongitude;

    private TextView mXAxis;
    private TextView mYAxis;
    private TextView mZAxis;

    private TextView mVehicleSpeed;
    private TextView mEngineRPM;
    private TextView mEngineTemperature;
    private TextView mThrottlePosition;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case Constants.MESSAGE_CONNECTING_DEVICE: {
                    String device = msg.getData().getString(Constants.CONNECTING_DEVICE);
                    Toast.makeText(MainActivity.this, "Connecting to " + device, Toast.LENGTH_SHORT).show();
                    break;
                }
                case Constants.MESSAGE_CONNECTED_DEVICE: {
                    String device = msg.getData().getString(Constants.CONNECTED_DEVICE);
                    Toast.makeText(MainActivity.this, "Connected to " + device, Toast.LENGTH_SHORT).show();

                    mObdHandler = new OBD(mBtService.getSocket(), MainActivity.this.mHandler);
                    mObdHandler.start();

                    mBtService = null;

                    break;
                }
                case Constants.MESSAGE_SPEED: {
                    String speed = msg.getData().getString(Constants.BT_RESPONSE);
                    mVehicleSpeed.setText(speed + "km/h");

                    break;
                }
                case Constants.MESSAGE_RPM: {
                    String rpm = msg.getData().getString(Constants.BT_RESPONSE);
                    mEngineRPM.setText(rpm + "rpm");

                    break;
                }
                case Constants.MESSAGE_THROTTLE: {
                    String throttle = msg.getData().getString(Constants.BT_RESPONSE);
                    mThrottlePosition.setText(throttle + "%");

                    break;
                }
                case Constants.MESSAGE_TEMP: {
                    String temp = msg.getData().getString(Constants.BT_RESPONSE);
                    mEngineTemperature.setText(temp + "°C");

                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate");

        mLatitude = (TextView) findViewById(R.id.latitude);
        mLongitude = (TextView) findViewById(R.id.longitude);

        mXAxis = (TextView) findViewById(R.id.xAxis);
        mYAxis = (TextView) findViewById(R.id.yAxis);
        mZAxis = (TextView) findViewById(R.id.zAxis);

        mVehicleSpeed = (TextView) findViewById(R.id.vehicleSpeed);
        mEngineRPM = (TextView) findViewById(R.id.engineRpm);
        mEngineTemperature = (TextView) findViewById(R.id.engineTemperature);
        mThrottlePosition = (TextView) findViewById(R.id.throttlePosition);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer_layout, R.string.close_drawer_layout) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mLocationProvider = new LocationProvider(this);
        mLocationProvider.addLocationCallback(this);

        mSensorProvider = new AccelerometerProvider(this);
        mSensorProvider.addSensorCallback(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        mLocationProvider.connect();
        mSensorProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");

        mLocationProvider.disconnect();
        mSensorProvider.disconnect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

        if (mObdHandler != null) {
            mObdHandler.terminate();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_bluetooth) {
            if (setupBluetooth()) {
                Intent btIntent = new Intent(this, BluetoothActivity.class);
                startActivityForResult(btIntent, REQUEST_CONNECT_DEVICE_SECURE);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "OnMapReady");

        mGoogleMap = googleMap;
        mGoogleMap.setMyLocationEnabled(true);

    }

    @Override
    public void onNewLocation(Location location) {
        Log.d(TAG, "onNewLocation");

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        mLatitude.setText(Double.toString(latitude));
        mLongitude.setText(Double.toString(longitude));

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16));
    }

    @Override
    public void onAccelerometerChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        mXAxis.setText(String.format("%.3f", x));
        mYAxis.setText(String.format("%.3f", y));
        mZAxis.setText(String.format("%.3f", z));
    }

    private boolean setupBluetooth() {
        Log.d(TAG, "setupBluetooth()");

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth adpater could not be found.", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult()");

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                if (resultCode == AppCompatActivity.RESULT_CANCELED) {
                    Toast.makeText(this, "No device selected", Toast.LENGTH_SHORT).show();
                } else if (resultCode == AppCompatActivity.RESULT_OK) {
                    String address = data.getStringExtra(BluetoothActivity.EXTRA_DEVICE_ADDRESS);

                    if (mBtService != null) {
                        mBtService.disconnect();
                    }

                    mBtService = new BluetoothService(mHandler);
                    mBtService.connect(mBtAdapter.getRemoteDevice(address));
                }
                break;
        }
    }
}
