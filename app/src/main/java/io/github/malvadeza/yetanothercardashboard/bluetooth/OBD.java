package io.github.malvadeza.yetanothercardashboard.bluetooth;

import io.github.malvadeza.yetanothercardashboard.Constants;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Antonio on 10/08/2015.
 */
public class OBD extends Thread {
    public static final int MESSAGE_READ = 2;

    private BluetoothSocket mBtSocket;
    private InputStream btInputStream;
    private OutputStream btOutputStream;

    private volatile boolean running = true;

    private Handler mHandler;

    public OBD(BluetoothSocket btSocket, Handler handler) {
        mBtSocket = btSocket;
        mHandler = handler;

        try {
            btInputStream = btSocket.getInputStream();
            btOutputStream = btSocket.getOutputStream();

            setDefaults();
            resetAll();
            echoOff();
            linefeedOff();
            spacesOff();
            headersOff();
            /* Others configuration, like linefeed and so on */
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getResponse() {
        StringBuilder res = new StringBuilder();
        byte buffer;

        try {
            while ( ((char)(buffer = (byte) btInputStream.read()) != '>')) {
                res.append((char) buffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }



        return res.toString().replaceAll("SEARCHING", "").replaceAll("\\s", "");
    }

    public void setDefaults() {
        String command = "AT D\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();

//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.BT_RESPONSE, response);
//
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_READ);
//        msg.setData(bundle);
//
//        mHandler.sendMessage(msg);
    }

    public void resetAll() {
        String command = "AT Z\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();

//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.BT_RESPONSE, response);
//
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_READ);
//        msg.setData(bundle);
//
//        mHandler.sendMessage(msg);
    }

    public void echoOff() {
        String command = "AT E0\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();

//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.BT_RESPONSE, response);
//
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_READ);
//        msg.setData(bundle);
//
//        mHandler.sendMessage(msg);
    }

    public void linefeedOff() {
        String command = "AT L0\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();

//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.BT_RESPONSE, response);
//
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_READ);
//        msg.setData(bundle);
//
//        mHandler.sendMessage(msg);
    }

    public void spacesOff() {
        String command = "AT S0\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();

//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.BT_RESPONSE, response);
//
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_READ);
//        msg.setData(bundle);
//
//        mHandler.sendMessage(msg);
    }

    public void headersOff() {
        String command = "AT H0\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();

//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.BT_RESPONSE, response);
//
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_READ);
//        msg.setData(bundle);
//
//        mHandler.sendMessage(msg);
    }

    @Override
    public void run() {
        while (running) {
            String speed = getSpeed();
            String rpm = getRPM();
            String throttle = getThrottle();
            String temp = getTemp();
        }
    }

    public void terminate() {
        this.running = false;
    }

    private String getSpeed() {
        String command = "01 0D\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();

        String ret;

        try {
            ret = Integer.toString(Integer.parseInt(response.substring(4), 16));
        } catch (Exception e) {
            ret = "0";
        }

        Bundle bundle = new Bundle();
        bundle.putString(Constants.BT_RESPONSE, ret);

        Message msg = mHandler.obtainMessage(Constants.MESSAGE_SPEED);
        msg.setData(bundle);

        mHandler.sendMessage(msg);

        return null;
    }

    private String getRPM() {
        String command = "01 0C\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();
        String ret;

        try {
            ret = Integer.toString(Integer.parseInt(response.substring(4), 16) / 4);
        } catch (Exception e) {
            ret = "0";
        }

        Bundle bundle = new Bundle();
        bundle.putString(Constants.BT_RESPONSE, ret);

        Message msg = mHandler.obtainMessage(Constants.MESSAGE_RPM);
        msg.setData(bundle);

        mHandler.sendMessage(msg);



        return ret;
    }

    private String getThrottle() {
        String command = "01 11\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();
        String ret;

        try {
            ret = Integer.toString(Integer.parseInt(response.substring(4), 16)*100/255);
        } catch (Exception e) {
            ret = "0";
        }

        Bundle bundle = new Bundle();
        bundle.putString(Constants.BT_RESPONSE, ret);

        Message msg = mHandler.obtainMessage(Constants.MESSAGE_THROTTLE);
        msg.setData(bundle);

        mHandler.sendMessage(msg);



        return ret;
    }

    private String getTemp() {
        String command = "01 05\r";

        try {
            btOutputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String response = getResponse();
        String ret;

        try {
            ret = Integer.toString(Integer.parseInt(response.substring(4), 16) - 40);
        } catch (Exception e) {
            ret = "0";
        }

        Bundle bundle = new Bundle();
        bundle.putString(Constants.BT_RESPONSE, ret);

        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TEMP);
        msg.setData(bundle);

        mHandler.sendMessage(msg);

        return ret;
    }
}
